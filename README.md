# Praktikumsprotokoll #

## Allgemeines ##

### build ###

Der *build*-Ordner wird (spätestens) beim Ausführen der Makefile erzeugt.
In diesem Ordner werden alle erzeugten Dateien, also Plots, Tabellen, PDFs, etc. gespeichert.

** *build* wird bei `make clean` ohne Nachfrage gelöscht, also keine wichtigen Dateien darin speichern!**

### data ###

In diesem Ordner sind alle verwendeten Messdaten gespeichert.

### media ###

In diesem Ordner sind alle im Dokument verwendeten Grafiken gespeichert.


## LaTeX ##

### metadata.tex ###

In die Datei *metadata.tex* kommt das Versuchsdatum, der Versuchsname und die Versuchsnummer.
Auf diese Variablen kann innerhalb des Dokuments folgendermaßen zugegriffen werden:

	:::latex hl_lines="1"
	Der Versuch \Versuchsnummer \enquote{\Versuchstitel} fand am \Versuchsdatum statt.

ergibt beispielsweise:

> Der Versuch V47 "Molwärme von Cu" fand am 16.06.2016 statt.

### main.tex ###

Die Datei *main.tex* ist die Hauptdatei die von LaTeX kompiliert werden muss.

### header.tex ###

Die Datei *header.tex* beinhaltet alle importierten Pakete und Einstellungen.

Benötigte Pakete können hier einfach eingefügt werden.


### shortcuts.tex ###

In der *shortcuts.tex* können häufig benötigte Befehle/Symbole/Zeichenketten als neuen Befehl angelegt werden.

Beispiel: Zugriff auf *E_{\gamma}* mit *\Egamma*

	:::latex

	\newcommand{E_{\gamma}}{\Egamma}

Mit Argument: Zugriff auf *\input{build/val_Messwert}* mit *\val{Messwert}*

	:::latex
	
	\newcommand{\val}[1]{\input{build/val_#1}}

### Grafik/Plot einbinden ###

	:::latex

	\begin{figure}
		\centering
		\includegraphics[width=.9\textwidth]{build/plot.pdf}
		\caption{Bildunterschrift.}
		\label{fig:plot}
	\end{figure}

Wichtig: Das Label darf erst **nach** der Caption definiert werden!

### Das *siunitx*-Paket ###

	:::latex

	\num{1.2} # Zahl
	\SI{1.2+-3e4}{\hertz} # Zahl mit Einheit
	\si{\meter\per\second\squared} # nur Einheit


## Python ##

### calc.py ###

Die Datei *calc.py* wird als Auswertungsskript der Messdaten verwendet.


### Messdaten ###

Messdaten bitte in entsprechenden Dateien im Ordner *data* ablegen.

Wenn die Messdaten, per Leerzeichen getrennt, in Spalten in einer Datei abgelegt sind, können sie folgendermaßen in Python importiert werden:

	:::python

	file = "data/Messdaten.txt"

	# jeder Datensatz in eine Variable
	x, y = np.genfromtxt(file, unpack=True)
	# oder alle Daten in einem Array
	data = np.genfromtxt(file, unpack=True)


### ufloat ###

	:::python

	x_mean = ufloat(np.mean(x), sem(x)) # Mittelwert von x mit Standardabweichung
	x = ufloat_fromstr('0.20+/-0.01')
	x = ufloat(0.2, 0.01) # Äquivalent zur Zeile darüber

siehe auch: https://pythonhosted.org/uncertainties/user_guide.html


### LaTeX-Tabellen ###

In Python verwendete Daten können ganz einfach in LaTeX-Tabellen exportiert werden.
Der folgende Code-Schnipsel exportiert die Daten in den Arrays `x` und `y` in die Datei *tbl_x_y.tex* im *build* Ordner.

	:::python

	lout.latex_table(name = 'x_y', # Wird gespeichert unter 'build/tbl_x_y.tex'. Tabelle bekommt das LaTeX-Label 'tbl:x_y'
		content = [x,y],
		col_title = r'$x$-Daten, $y$-Daten',
		col_unit = r'\meter,\milli\meter',
		fmt = '4.1,4.1', # 4.1 erzeugt: 4 Zeichen vor dem Komma und 1 Nachkommastelle
		caption = r'Ein x-y-Plot.')

Die Tabelle kann dann folgendermaßen in ein LaTeX-Dokument eingebunden werden:

	:::latex

	\tbl{x_y}

Die Caption muss dabei im Python-Skript definiert werden. Das Label der Tabelle ist *tbl:<NAME>*.


### LaTeX-Werte ###

Auch berechnete Werte können einfach in LaTeX exportiert werden. Der Vorteil ist, dass hierbei weniger Fehler als bei copy+paste entstehen können.
Außerdem wird der Wert im LaTeX-Dokument bei jeder Änderung der Berechnung im Skript angepasst.

Für den Export der Variable `x` in die Datei *x.tex* im Ordner *build*

	:::python

	lout.latex_val(name='x',
		val=x,
		unit='mm',
		digits=1)

Die Werte können beispielsweise auf folgende Weise in das LaTeX-Dokument eingebunden werden:

	:::latex

	\begin{equation}
		x = \val{x}
	\end{equation}


### Fitten ###

Ein linearer Fit an die Wertepaare {x,y} kann mit der Funktion `curve_fit()` durchgeführt werden.
Dabei ist zu beachten, dass die zu fittende Funktion **vor** dem Aufruf von `curve_fit()` definiert wird.
Mit dem Parameter `p0` (optional) können Startwerte für die Fitparameter übergeben werden.
Mit `sigma` (optional) können die Fehler der y-Werte übergeben werden.

	:::python

	def f(x, a, b):
		return a * x + b

	par, cov = curve_fit(f, x, y, p0=(a_start, b_start), sigma=y_err)
	err = np.sqrt(np.diag(cov))

	fit_a = ufloat(par[0], err[0])
	fit_b = ufloat(par[1], err[1])
	par_f = par

In den Arrays `par` und `err` sind die best-fit-Parameter sowie -Fehler gespeichert.
Zum späteren Zugriff werden die Werte (mit Fehlern) in die ufloats `fit_a` und `fit_b` geschrieben.
Für einfacheres Plotten (s.u.) wird außerdem `par` in `par_f` gespeichert.


### Plotten ###

Zum Plotten kann matplotlib verwendet werden.

Einzelner Plot:

	:::python

	x_offset = (np.max(x) - np.min(x)) * 0.05
	x_plot = np.linspace(np.min(x) - x_offset, np.max(x) + x_offset, 100)

	fig = plt.figure()

	# Einfacher Plot
	plt.plot(x, y, 'k.', label='Messdaten')

	# Plot mit Fehlerbalken
	plt.errorbar(x, y, xerr=x_err, yerr=y_err, fmt='k.', label='Messwerte mit Fehlern')

	# Plot einer Ausgleichs- oder Theoriefunktion f
	plt.plot(x_plot, f(x_plot, *par_f, 'C0-', label='Fit')

	plt.xlabel('x')
	plt.ylabel('y')
	plt.xlim(np.min(x_plot), np.max(x_plot))
	plt.legend(loc='best')
	plt.savefig('build/plt_x-y-Plot.pdf')
	plt.close()
	
Der erzeugte Plot wird als *plt_x-y-Plot.pdf* im *build*-Ordner gespeichert.

Plot mit Verhältnis-Subplot:

	:::python

	fig, (ax1, ax2) = plt.subplots(2, sharex=True)

	# Großer Hauptplot
	ax1.errorbar(x, y, yerr=yerr, fmt='k.', label='Messdaten')
	ax1.plot(x_plot, f(x_plot,*par_f), 'C3-', label='Fit')
	ax1.set_ylabel('y')
	ax1.legend()
	
	# Kleiner Verhältnisplot darunter
	ax2.plot(x_plot, np.ones_like(x_plot), 'C3-')
	ax2.errorbar(x, y / f(x, *par_f), yerr=yerr / f(x, *par_f), fmt='k.')
	ax2.set_xlabel('x')
	ax2.set_ylabel('Verhältnis')
	ax2.set_xlim(np.min(x_plot), np.max(x_plot))
	ratio_xrange = .81
	ax2.set_ylim(1 - ratio_xrange, 1 + ratio_xrange)
	
	# Positionen und Größen anpassen
	ax1.set_position([.1, .31, .8, .6])
	ax2.set_position([.1, .1, .8, .2])
	
	plt.savefig('build/plt_x-y-Ratio-Plot.')
	plt.close()


## Git ##

Immer **vor** dem Beginn der Bearbeitung einmal den aktuellen Stand vom Server holen:

	:::bash

	git pull

Nach der Bearbeitung: Zum Index hinzufügen, Committen, Pushen

	:::bash

	git add --all # fügt alle Dateien zum Index hinzu, auch explizite Angabe von Dateinamen möglich
	git commit -m "COMMIT_MESSAGE"
	git pull # Zur Sicherheit vor dem pushen nochmal pullen
	git push

## Make ##

Das Praktikumsprotokoll ist mit einer Makefile konfiguriert.  
Folgende Befehle stehen zur Verfügung

* `make` oder `make all` Führt alle Python Dateien aus und erstellt danach die PDF Datei
* `make pdf` Erstellt nur die PDF Datei und ignoriert Änderungen am Python Skript
* `make pvc` wie `make pdf`. Läuft im Hintergrund weiter und erzeugt1 bei jeder Änderung die PDF Datei neu.
* `make py` Führt alle Python Dateien im Verzeichnis aus
* `make clean` Löscht den build und den \_\_pycache\_\_ Ordner
\input{header.tex}
\input{metadata.tex}
\input{shortcuts.tex}

\author{
  Tim Kallage    \\    tim.kallage@tu-dortmund.de \and
  Christian Geister \\ christian.geister@tu-dortmund.de
}
\title{(\Versuchsnummer)\\\Versuchstitel}
\date{Durchführung: \Versuchsdatum}

\begin{document}
\maketitle
\thispagestyle{empty}
\vfill
\tableofcontents
\newpage

%------------------------------------------------
\section{Motivation}
%------------------------------------------------
In diesem Versuch soll die Aufspaltung der Spektrallinien durch den Zeeman-Effekt untersucht werden. Diese Aufspaltung wird durch ein externes Magnetfeld hervorgerufen.

%------------------------------------------------
\section{Theoretische Grundlagen}
%------------------------------------------------
Beim Übergang von einem höheren Energieniveau zu einem tieferen, sendet ein Atom eine charakteristische Strahlung aus, dessen Wellenlänge der Energiedifferenz entspricht.
In einem Magnetfeld spalten nun diese Spektrallinien auf.
Um den Zeeman-Effekt zu erklären sind folgende Überlegungen notwendig.

Ein Hüllenelektron besitzt zwei Drehimpulsartige Quantenzahlen.
Dieses ist zum einen der Spin $s$ und der Bahndrehimpuls $l$.
Der Betrag dieser Drehimpulsvektoren ist durch die Lösung der Eigenwertgleichung gegeben durch
\begin{align}
|\vec{l}|&=\sqrt{l(l+1)}\hbar &\quad &\text{mit}\quad l = 0, 1, .., n-1\\
|\vec{s}|&=\sqrt{s(s+1)}\hbar &\quad &\text{mit}\quad s = \frac{1}{2}
\end{align}
wobei $n$ die Hauptquantenzahl des Atoms ist.
Auf Grund der Ladung des Elektrons ergeben sich daraus jeweils magnetische Momente
\begin{align}
\vec{\mu}_l &= -\mu_\mathrm{B}\sqrt{l(l+1)}\hat{l}\\
\vec{\mu}_s &= -g_s\mu_\mathrm{B}\sqrt{s(s+1)}\hat{s}.
\end{align}
Der Lande-Faktor $g_s$ folgt aus der relativistischen Betrachtung des Elektrons und kann als $\approx 2$ angenommen werden.

Die Bahndrehimpulse und Spins der einzelnen Elektronen müssen nun vektoriell addiert werden. Dabei kann auf abgeschlossene Schalen verzichtet werden, da sich dort alle Drehimpulse gegenseitig aufheben. Man erhält den Gesamtbahndrehimpuls $\vec{L}$ sowie den Gesamtspin $\vec{S}$. Es ist zweckmäßig den Gesamtdrehimpuls zu betrachten. Für niedrige Kernladungszahlen ist die Wechselwirkung der Drehimpulse untereinander groß gegenüber der Wechselwirkung von Bahndrehimpuls und Spin. Somit kann für den Gesamtdrehimpuls
\begin{equation}
\vec{J} = \vec{L} + \vec{S}
\end{equation}
berechnet werden. Dies ist die so genannte Spin-Bahn-Kopplung.
Für große Kernladungszahlen muss zunächst für jedes Elektron einzeln die Summe aus Spin und Bahndrehimpuls gebildet werden und anschließend werden diese zum Gesamtdrehimpuls summiert.

Um die Energieniveaus eines Atoms zu benennen wird die Notation
\begin{equation}
 ^{M}L_J
\end{equation}
verwendet. Dabei wird der Bahndrehimpuls $L$ mit den Buchstaben $S, P, D, F$ bezeichnet.
Die Quantenzahl $M$ ergibt sich aus $M=2S+1$.

Die magnetischen Momente werden ebenfalls vektoriell addiert.
Für den Betrag des magnetischen Moments für den Gesamtdrehimpuls $J$ ergibt sich
\begin{equation}
|\mu_J|\approx \mu_\mathrm{B} g_J \sqrt{J(J+1)}.
\end{equation}
Der entsprechende Lande-Faktor ist gegeben durch
\begin{equation}\label{eq:gj}
g_J = \frac{3(J(J+1)+S(S+1)-L(L+1)}{2J(J+1)}.
\end{equation}
In einem externen Magnetfeld kommt es nun zur Richtungsquantelung.
Dabei ändert sich die $z$-Komponente des magnetischen Moments
\begin{equation}
\mu_{J_z} = -m g_J \mu_\mathrm{B}
\end{equation}
jeweils nur um ganzzahlige Vielfache, die durch $m$ abgezählt werden. $m$ läuft dabei von $-J$ bis $J$.
Es ergibt sich also eine Energieaufspaltung, wie sie in Abbildung \ref{fig:aufspaltung} für $J=2$ dargestellt ist.
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{media/aufspaltung.png}
\caption{Aufspaltung eines Energieniveaus eines Atoms mit der Gesamtdrehimpulsquantenzahl $J=2$. \cite{Anleitung}}
\label{fig:aufspaltung}
\end{figure}
Diese Aufspaltung tritt bei allen Energieniveaus auf.
Es ergeben sich damit neue Übergänge, die jedoch durch Auswahlregeln für die Quantenzahl $m$ eingeschränkt sind.
Übergänge mit $\Delta m=0$ (so genannte $\pi$-Übergänge) sind erlaubt, wobei dabei linear zur Feldrichtung polarisiertes Licht ausgesendet wird.
Für $\Delta m=\pm1$ erhält man zirkular Polarisiertes Licht zur Feldrichtung.
Diese Übergänge werden als $\sigma$-Übergänge bezeichnet.
Alle anderen Übergänge sind verboten.

\subsection{Normaler und anomaler Zeeman-Effekt}
Beim Spezialfall in dem der Gesamtspin verschwindet, spricht man vom normalen Zeeman-Effekt.
Hierbei ist $g_J=1$ für alle $J$. Damit sind alle Energieaufspaltungen äquidistant.
Für $\Delta m=-1,0,+1$ beobachtet man daher eine Aufspaltung in drei Linien.

Beim anomalen Zeeman-Effekt ist der Spin von 0 verschieden.
Damit ergeben sich unterschiedliche Lande-Faktoren, die dazu führen, dass mehr Linien beobachtet werden.
Die Energiedifferenz ergibt sich für zwei Niveaus zu
\begin{equation} \label{eq:dE}
\Delta E = (m_1 g_1 - m_2 g_2)\mu_\mathrm{B} B.
\end{equation}
Die Auswahlregeln für $m$ bleiben bestehen und $g_i$ hängt nun von $L_i, S_i, J_i$ ab.

%------------------------------------------------
\section{Aufbau und Durchführung}
%------------------------------------------------
Es wird eine Cadmium-Lampe als Quelle verwendet.
Die rote Spektrallinie entsteht durch Übergänge mit $S=0$, so dass hier der normale Zeeman-Effekt beobachtet werden kann.
Außerdem besitzt das Cadmium-Spektrum eine blaue Linie, für die $S=1$ gilt.
Es kann also auch der anomale Zeeman-Effekt untersucht werden.

Die Lampe ist in einen Elektromagneten eingebaut.
Das ausgesendete Licht wird kollimiert und mit einem Polarisationsfilter der zu untersuchende Übergang ausgewählt.
Mit einem Spalt wird die gewünschte Wellenlänge herausgegriffen.
Das Licht wird mit einer Lummer-Gehrcke-Platte zur Interferenz gebracht und das entstehende Muster mit einer Digitalkamera aufgenommen.
Der Gesamte Aufbau ist in Abbildung \ref{fig:aufbau} dargestellt.
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{media/aufbau.png}
\caption{Schematischer Aufbau der Messapparatur. \cite{Anleitung}}
\label{fig:aufbau}
\end{figure}

Eine Lummer-Gehrcke-Platte besteht aus einer parallelen Platte in der Licht reflektiert und teilweise transmittiert wird.
Für das austretende Licht ergibt sich eine Interferenzbedingung
\begin{equation}
2d\cos\theta=n\lambda
\end{equation}
mit der Dicke $d$, der Wellenlänge $\lambda$ und der Ordnung $n$.
Eine Änderung der Wellenlänge durch das Magnetfeld führt also zu einer Verschiebung der Interferenzstreifen.
Die maximale Wellenlängendifferenz um sich nicht zu überlagern ist
\begin{equation} \label{eq:lamD}
\Delta \lambda_\mathrm{D} = \frac{\lambda^2}{2d}\sqrt{\frac{1}{n^2-1}},
\end{equation}
hier mit dem Brechnungsindex der Lummer-Gehrcke-Platte $n$.
Das Auflösungsvermögen hängt von der Länge $L$ ab und ergibt sich zu
\begin{equation} \label{eq:A}
A = \frac{\lambda}{\Delta \lambda}=\frac{L}{\lambda}(n^2-1).
\end{equation}

%------------------------------------------------
\section{Auswertung}
%------------------------------------------------
\subsection{Auflösungsvermögen der Lummer-Gehrcke-Platte und
Dispersionsgebiet}
%------------------------------------------------
Die Lummer-Gehrke-Platte besitzt eine Dicke von $d = \SI{4}{\milli\meter}$
und eine Länge von $\SI{120}{\milli\meter}$.
Der Brechungsindex für die Wellenlänge der roten Linie,
$\lambda_1 = \SI{644}{\nano\meter}$, beträgt $n_1 = 1.4567$, für die blaue
Linie mit $\lambda_2 = \SI{480}{\nano\meter}$ ist $n_2 = 1.4635$.
Mit Formel \eqref{eq:A} ergibt sich dann für das Auflösungsvermögen
bei der roten Linie $A_1 = \val{rot_A}$ und bei der blauen Linie
$A_2 = \val{blau_A}$.
Mit den Werten der Lummer-Gehrke-Platte ergibt sich nach \eqref{eq:lamD}
außerdem für das Dispersionsgebiet bei der roten Linie
$\lambda_\mathrm{D1} = \val{rot_Delta_lambda}$ und bei der blauen Linie
$\lambda_\mathrm{D2} = \val{blau_Delta_lambda}$.
%------------------------------------------------
\subsection{Kalibrierung des Elektromagneten}
%------------------------------------------------
Zur Bestimmung der Abhängigkeit der Feldstärke $B$ des Elektromagneten vom
Strom $I$ wird der Strom im Bereich von 0 bis \SI{15}{\ampere} variiert
und das Magnetfeld jeweils mit einer Hallsonde ausgemessen.
Im vermessenen Bereich ergibt sich näherungsweise eine lineare Beziehung
zwischen $I$ und $B$.
\begin{equation} \label{eq:Bcal_reg}
  B(I) = \val{Bcal_a}\times I + \val{Bcal_b}
\end{equation}
In Abb.~\ref{fig:Bcal} sind die Messwerte und die Regressionsgerade dargestellt.
\begin{figure}[tb]
  \centering
  \includegraphics[width=.8\textwidth]{build/plt_Bcal.png}
  \caption{Gemessene Feldstärke $B$ gegen angelegten Strom $I$,
  dargestellt zusammen mit der linearen Regressionsgeraden
  (Werte in Tab.~\ref{tbl:Bcal}).}
  \label{fig:Bcal}
\end{figure}
%------------------------------------------------
\subsection{Allgemeines zur Messung der Landé-Faktoren}
%------------------------------------------------
Aus dem mit der Digitalkamera aufgenommenen Bildern werden zwei Größen
bestimmt.
Zum einen $\Delta s$, was dem Abstand der einzelnen Interferenzlinien
voneinander bei ausgeschaltetem Magnetfeld entspricht.
Des weiteren $\delta s$, dem Abstand zwischen den beiden Aufspaltungslinien
bei eingeschaltetem Magnetfeld.
Daraus lässt sich die Aufspaltung der Spektrallinie,
\begin{equation} \label{eq:dlambda}
  \delta\lambda = \frac{1}{2}\frac{\delta s}{\Delta s}
  \Delta \lambda_\mathrm{D},
\end{equation}
mit dem Dispersionsgebiet $\Delta \lambda$, bestimmen.
Mit $E = \frac{hc}{\lambda}$ ergibt sich für die Ableitung
\begin{equation}
  \frac{\mathrm{d}E}{\mathrm{d}\lambda} = -\frac{hc}{\lambda^2},
\end{equation}
was näherungsweise umgeformt werden kann zu
\begin{equation} \label{eq:absdE1}
  |\Delta E| = \frac{hc}{\lambda^2}|\Delta \lambda|.
\end{equation}
Nach \eqref{eq:dE} gilt außerdem
\begin{equation} \label{eq:absdE2}
  |\Delta E| = |\Delta m|g_J\mu_\mathrm{B}B .
\end{equation}
Aus der Kombination von \eqref{eq:absdE1} und \eqref{eq:absdE2} folgt dann
mit $|\Delta \lambda| = \delta\lambda$
\begin{equation}
  g_J = \frac{hc}{\lambda^2\mu_\mathrm{B}B} \delta \lambda,
\end{equation}
und mit \eqref{eq:dlambda} dann schließlich
\begin{equation} \label{eq:gj_messung}
  g_J = \frac{hc\Delta\lambda_\mathrm{D}}
             {2 \lambda^2\mu_\mathrm{B}B}\frac{\delta s}{\Delta s}
\end{equation}

Die rote Spektrallinie der Cadmium-Lampe stellt den Übergang
${}^1\mathrm{D}_2 \rightarrow {}^1\mathrm{P}_1$ dar.
Die Landé-Faktoren ergeben sich nach Formel \eqref{eq:gj} für beide Niveaus
zu $g_J = 1$.
Da für den $\pi$-Übergang $\Delta m = 0$ ist, ergibt sich nach Formel
\eqref{eq:dE} hier keine Energieaufspaltung.
Die $\sigma$-Übergänge mit $\Delta m = \pm1$ erzeugen jedoch eine
Aufspaltung der Linien um $\Delta E = \pm \mu_\mathrm{B} B$.
Die blaue Spektrallinie stellt den Übergang
${}^3\mathrm{P}_1 \rightarrow {}^3\mathrm{S}_1$ dar.
Nach \eqref{eq:gj} ergibt sich für das ${}^3\mathrm{P}_1$-Niveau ein
Laudé-Faktor von $g_J = \frac{3}{2}$, für ${}^3\mathrm{S}_1$ jedoch
$g_J = 2$.
Mit \eqref{eq:dE} ergibt sich für den $\pi$-Übergang
$\Delta E = \pm \frac{1}{2}\mu_\mathrm{B}B$, für die $\sigma$-Übergänge
ergeben sich jedoch $\Delta E = \pm \frac{3}{2}\mu_\mathrm{B}B$ und
$\Delta E = \pm 2\mu_\mathrm{B}B$.
Da in diesem Versuch die verschiedenen, aufgespaltenen $\sigma$-Linien
nicht voneinander differenziert werden können, wird die Energieaufspaltung
mit $\Delta E = \pm 1.75\mu_\mathrm{B}B$ approximiert.
%------------------------------------------------
\subsection{Messung des Laudé-Faktors für die $\sigma$-Aufspaltung
der roten Spektrallinie}
%------------------------------------------------
\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{build/plt_peak_img_rot.png}
  \caption{Aufgenommenes Interferenzbild hinter der Lemmer-Gehrcke-Platte
  ohne (oben) und mit (unten) angelegtem Magnetfeld für die $\sigma$-Aufspaltung der roten Spektrallinie.
  In das obere Bild sind außerdem die abgelesenen $\Delta s$,
  unten die zugehörigen $\delta s$ eingezeichnet.}
  \label{fig:peak_img_rot}
\end{figure}
\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{build/plt_rot_g.png}
  \caption{Gemessene Laudé-Faktoren für jede vermessene
  Interferenzlinie des $\sigma$-Übergangs der roten Spektrallinie,
  dargestellt zusammen mit dem Mittelwert und dessen statistischen Fehler.}
  \label{fig:rot_g}
\end{figure}
Für die Aufnahme der aufgespalteten Linien wird ein Strom von
$I = \val{rot_I}$ angelegt.
Dieser Strom entspricht nach \eqref{eq:Bcal_reg} einer magnetischen
Feldstärke von
\begin{equation}
  B = \val{rot_B}.
\end{equation}
Die aufgenommenen Interferenzmuster sind in Abb.~\ref{fig:peak_img_rot}
dargestellt, zusammen mit den abgelesenen $\Delta s$ (oben) und $\delta s$
(unten).
Die nach \eqref{eq:gj_messung} ermittelten Laudé-Faktoren sind in
Abb.~\ref{fig:rot_g} für jede vermessene Interferenzlinie dargestellt.
Der Mittelwert ergibt sich zu
\begin{equation}
  g_{J1,\sigma} = \val{rot_g}
\end{equation}
%------------------------------------------------
\subsection{Messung des Laudé-Faktors für die $\sigma$-Aufspaltung
der blauen Spektrallinie}
%------------------------------------------------
\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{build/plt_peak_img_blau_sigma.png}
  \caption{Aufgenommenes Interferenzbild hinter der Lemmer-Gehrcke-Platte
  ohne (oben) und mit (unten) angelegtem Magnetfeld für die $\sigma$-Aufspaltung der blauen Spektrallinie.
  In das obere Bild sind außerdem die abgelesenen $\Delta s$,
  unten die zugehörigen $\delta s$ eingezeichnet.}
  \label{fig:peak_img_blau_sigma}
\end{figure}
\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{build/plt_blau_sigma_g.png}
  \caption{Gemessene Laudé-Faktoren für jede vermessene
  Interferenzlinie des $\sigma$-Übergangs der blauen Spektrallinie,
  dargestellt zusammen mit dem Mittelwert und dessen statistischen Fehler.}
  \label{fig:blau_sigma_g}
\end{figure}
Für die Aufnahme der aufgespalteten Linien wird ein Strom von
$I = \val{blau_sigma_I}$ angelegt.
Dieser Strom entspricht nach \eqref{eq:Bcal_reg} einer magnetischen
Feldstärke von
\begin{equation}
  B = \val{blau_sigma_B}.
\end{equation}
Die aufgenommenen Interferenzmuster sind in Abb.~\ref{fig:peak_img_blau_sigma}
dargestellt, zusammen mit den abgelesenen $\Delta s$ (oben) und $\delta s$
(unten).
Die nach \eqref{eq:gj_messung} ermittelten Laudé-Faktoren sind in
Abb.~\ref{fig:blau_sigma_g} für jede vermessene Interferenzlinie dargestellt.
Der Mittelwert ergibt sich zu
\begin{equation}
  g_{J2,\sigma} = \val{blau_sigma_g}
\end{equation}
%------------------------------------------------
\subsection{Messung des Laudé-Faktors für die $\pi$-Aufspaltung
der blauen Spektrallinie}
%------------------------------------------------
\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{build/plt_peak_img_blau_pi.png}
  \caption{Aufgenommenes Interferenzbild hinter der Lemmer-Gehrcke-Platte
  ohne (oben) und mit (unten) angelegtem Magnetfeld für die $\pi$-Aufspaltung der blauen Spektrallinie.
  In das obere Bild sind außerdem die abgelesenen $\Delta s$,
  unten die zugehörigen $\delta s$ eingezeichnet.}
  \label{fig:peak_img_blau_pi}
\end{figure}
\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{build/plt_blau_pi_g.png}
  \caption{Gemessene Laudé-Faktoren für jede vermessene
  Interferenzlinie des $\pi$-Übergangs der blauen Spektrallinie,
  dargestellt zusammen mit dem Mittelwert und dessen statistischen Fehler.}
  \label{fig:blau_pi_g}
\end{figure}
Für die Aufnahme der aufgespalteten Linien wird ein Strom von
$I = \val{blau_pi_I}$ angelegt.
Dieser Strom entspricht nach \eqref{eq:Bcal_reg} einer magnetischen
Feldstärke von
\begin{equation}
  B = \val{blau_pi_B}.
\end{equation}
Die aufgenommenen Interferenzmuster sind in Abb.~\ref{fig:peak_img_blau_pi}
dargestellt, zusammen mit den abgelesenen $\Delta s$ (oben) und $\delta s$
(unten).
Die nach \eqref{eq:gj_messung} ermittelten Laudé-Faktoren sind in
Abb.~\ref{fig:blau_pi_g} für jede vermessene Interferenzlinie dargestellt.
Der Mittelwert ergibt sich zu
\begin{equation}
  g_{J2,\pi} = \val{blau_pi_g}
\end{equation}

%------------------------------------------------
\section{Zusammenfassung und Diskussion}
%------------------------------------------------
Für die Laudé-Faktoren ergeben sich in dieser Messung folgende Werte:

\begin{table}[htb]
  \caption{Zusammenfassung aller gemessenen Laudé-Faktoren}
  \label{tbl:summary}
  \centering
  \begin{tabular}{
    r
    c
    S
    S[table-format=1.3,table-figures-uncertainty=1]
    S[table-format=2.1,retain-explicit-plus]}
  \toprule
  Übergang & & {Theoriewert $g_J$} & {Messwert $g_J$} & {Abweichung} \\
  \midrule
    {rot (${}^1\mathrm{D}_2 \rightarrow {}^1\mathrm{P}_1$)} &
      $\sigma$ & 1 & 1.140+-0.009 & +14.0\,\% \\
    \midrule
    \multirow{2}{*}{blau (${}^3\mathrm{P}_1 \rightarrow {}^3\mathrm{S}_1$)} &
      $\sigma$ & 1.75 & 2.127+-0.032 & +21.5\,\% \\
    & $\pi$ & 0.5 & 0.559+-0.006 & +11.8\,\% \\
  \bottomrule
  \end{tabular}
\end{table}

Die Abweichung in allen Messwerten um 10 bis \SI{20}{\percent} nach oben
deutet auf einen systematischen Fehler hin.
Das Magnetfeld könnte beispielsweise dafür verantwortlich sein.
Ebenfalls könnten Fehler beim Ablesen der $\delta s$ aufgetreten sein.
%------------------------------------------------
\appendix
\numberwithin{table}{section}%
\section{Anhang}
%------------------------------------------------
\tbl{Bcal}
%------------------------------------------------
% LITERATURVERZEICHNIS
%------------------------------------------------
\begin{thebibliography}{xx}
\small
\bibitem{Anleitung}
	TU Dortmund,
	\textit{Versuch \Versuchsnummer~- \Versuchstitel},
	\url{http://129.217.224.2/HOMEPAGE/Anleitung_FPMa.html},
	abgerufen am \Versuchsdatum.

\end{thebibliography}

\end{document}

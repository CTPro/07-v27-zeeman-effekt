import numpy as np
import matplotlib.pyplot as plt
import latexoutput as lout
print("Using latexoutput, Version", lout.__version__)

"""comment out what you need"""
#import matplotlib as mpl
#mpl.rc('axes', {'formatter.useoffset': False, 'formatter.limits': [-5, 5]})
from scipy.optimize import curve_fit
from scipy.stats import sem # standard error of mean
from uncertainties import ufloat#, ufloat_fromstr
import uncertainties.unumpy as unp
from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
import scipy.constants as const
#from scipy.interpolate import interp1d
#from scipy.integrate import simps, quad, trapz
import scipy
from scipy import ndimage
from matplotlib import cm

lout._output_level = 1

mu_B = const.physical_constants["Bohr magneton"][0]
c = const.c
h = const.h

LGP_d = 4e-3  # 4 mm
LGP_L = .12  # 120 mm
LGP_n2_rot = 1.4567**2  # n^2 bei rotem Licht
LGP_n2_blau = 1.4635**2  # n^2 bei blauem Licht

rot_lambda = 643.8e-9  # Wellenlänge des roten Lichts
blau_lambda = 480e-9  # Wellenlänge des blauen Lichts

# Daten importieren
print("Lade Daten")
im_rot_null = scipy.misc.imread("data/rot_null.JPG", "F")
im_rot_aufspaltung = scipy.misc.imread("data/rot_aufspaltung.JPG", "F")
im_blau_sigma_null = scipy.misc.imread("data/blau_sigma_null.JPG", "F")
im_blau_sigma_aufspaltung = scipy.misc.imread(
    "data/blau_sigma_aufspaltung.JPG", "F")
im_blau_pi_null = scipy.misc.imread("data/blau_pi_null.JPG", "F")
im_blau_pi_aufspaltung = scipy.misc.imread("data/blau_pi_aufspaltung.JPG",
                                           "F")

rot_null_peaks = np.genfromtxt(
    "data/peaks_rot_null.txt", unpack=True)
rot_aufspaltung_peaks = np.genfromtxt(
    "data/peaks_rot_aufspaltung.txt", unpack=True)
blau_sigma_null_peaks = np.genfromtxt(
    "data/peaks_blau_sigma_null.txt", unpack=True)
blau_sigma_aufspaltung_peaks = np.genfromtxt(
    "data/peaks_blau_sigma_aufspaltung.txt", unpack=True)
blau_pi_null_peaks = np.genfromtxt(
    "data/peaks_blau_pi_null.txt", unpack=True)
blau_pi_aufspaltung_peaks = np.genfromtxt(
    "data/peaks_blau_pi_aufspaltung.txt", unpack=True)

Bcal_I, Bcal_B = np.genfromtxt("data/data_Bcal.txt", unpack=True)

rot_I = np.genfromtxt("data/data_rot_I.txt")
blau_sigma_I = np.genfromtxt("data/data_blau_sigma_I.txt")
blau_pi_I = np.genfromtxt("data/data_blau_pi_I.txt")

# Werte aus Vorbereitung berechnen

rot_Delta_lambda = rot_lambda**2 / (2 * LGP_d) * np.sqrt(1 / (LGP_n2_rot - 1))
blau_Delta_lambda = blau_lambda**2 / (2 * LGP_d) * np.sqrt(1 /
                                                           (LGP_n2_blau - 1))

rot_A = LGP_L / rot_lambda * (LGP_n2_rot - 1)
blau_A = LGP_L / blau_lambda * (LGP_n2_blau - 1)

lout.latex_val("rot_Delta_lambda", rot_Delta_lambda * 1e12, r"\pico\meter",
               digits=3)
lout.latex_val("blau_Delta_lambda", blau_Delta_lambda * 1e12, r"\pico\meter",
               digits=3)
lout.latex_val("rot_A", rot_A, digits=0)
lout.latex_val("blau_A", blau_A, digits=0)

# Hysteresekurve des Elektromagneten
def lin(x, a, b):

    return a * x + b

print("Kalibriere Elektromagnet")

par, cov = curve_fit(lin, Bcal_I, Bcal_B)
err = np.sqrt(np.diag(cov))

fig, ax = plt.subplots()
ax.plot(Bcal_I, lin(Bcal_I, *par) * 1e3, label="Regressionsgerade")
ax.plot(Bcal_I, Bcal_B*1e3, "k.", label="Messwerte")
ax.set_xlabel("Strom $I$ [A]")
ax.set_ylabel("Magnetfeld $B$ [mT]")
plt.savefig("build/plt_Bcal.png")

lout.latex_val("Bcal_a", ufloat(par[0], err[0]) * 1e3,
               r"\milli\tesla\per\ampere")
lout.latex_val("Bcal_b", ufloat(par[1], err[1]) * 1e3,
               r"\milli\tesla")
lout.latex_table("Bcal", [Bcal_I, Bcal_B * 1e3],
                 col_title=["Stromstärke $I$",
                            "Feldstärke $B$"],
                 col_unit=[r"\ampere", r"\milli\tesla"],
                 fmt=["2.0", "3.0"],
                 caption="Messwerte der Magnetfeld-Kalibrierung.")

# Auswertungs-Funktionen

def get_ds_Delta_s(peaks_null, peaks_aufspaltung):
    #Delta_s = 0.5 * (peaks_null[2:] - peaks_null[:-2])
    Delta_s = (peaks_null[1:] - peaks_null[:-1])
    ds = peaks_aufspaltung[1::2] - peaks_aufspaltung[:-1:2]

    return ds, Delta_s


def save_peak_plot(spectrum, peaks, name):

    fig, ax = plt.subplots()
    ax.plot(spectrum, "k-", lw=1)
    for peak in peaks:
        ax.axvline(peak, c="C3", lw=1)
    ax.set_ylim(0, ax.get_ylim()[1])
    plt.savefig("build/plt_spectrum_{}.png".format(name), dpi=200)
    plt.close()
    lout.latex_table(
        content=[peaks],
        name="peaks_{}".format(name),
        col_title="Peakposition [px]",
        fmt="4.0",
        caption=("Peaks der Messung \\texttt{"
                 + name.replace("_", "\\_") + "}."))


def save_peak_img(im_null, im_aufspaltung, peaks_null, peaks_aufspaltung,
                  name):

    print("save_peak_img(): Saving " + name)
    im_null = im_null[1000: -1000, :]
    im_aufspaltung = im_aufspaltung[1000: -1000, :]
    leny, lenx = np.shape(im_null)
    fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True)
    # draw im_null
    ax1.imshow(im_null, cmap=cm.bone, aspect="auto")
    ax1.set_yticks([])
    ax1.set_xticks([])
    for j in range(len(peaks_null) - 1):
        if (j % 2 == 0):
            yline = .35
        else:
            yline = .65
        ax1.axvline(peaks_null[j], yline - .05, yline + .05,
                      c="C3", lw=1)
        ax1.axvline(peaks_null[j + 1], yline - .05, yline + .05,
                      c="C3", lw=1)
        ax1.axhline((1 - yline) * leny,
                      peaks_null[j] / lenx,
                      peaks_null[j + 1] / lenx, c="C3", lw=1)
        ax1.annotate("{:.0f}".format(.5 * (peaks_null[j + 1] - peaks_null[j])),
                       xy=(.5 * (peaks_null[j] + peaks_null[j + 1]),
                       (.5 * (1 - yline) + .25) * leny), xycoords='data',
                       color="C1", ha="center", va="center")
    # draw im_aufspaltung
    ax2.imshow(im_aufspaltung, cmap=cm.bone, aspect="auto")
    ax2.set_yticks([])
    ax2.set_xticks([])
    for j in range(len(peaks_aufspaltung) - 1):
        if (j % 2 == 0):
            ax2.axvline(peaks_aufspaltung[j], 0.45, 0.55, c="C3", lw=1)
            ax2.axvline(peaks_aufspaltung[j + 1], 0.45, 0.55, c="C3", lw=1)
            ax2.axhline(leny / 2, peaks_aufspaltung[j] / lenx,
                          peaks_aufspaltung[j + 1] / lenx, c="C3", lw=1)
            ax2.annotate(
                "{:.0f}".format(
                    .5 * (peaks_aufspaltung[j + 1] - peaks_aufspaltung[j])),
                xy=(.5 * (peaks_aufspaltung[j] + peaks_aufspaltung[j + 1]),
                    .6 * leny),
                xycoords='data', color="C1", ha="center", va="center")

    ax1.set_position([.05, .5, .9, .45])
    ax2.set_position([.05, .05, .9, .45])
    plt.savefig("build/plt_peak_img_{}.png".format(name), dpi=200)


def calc_g(ds, Delta_s, lam, Delta_lambda, I, name):

    B = lin(I, *par)

    dlambda = 0.5 * ds / Delta_s * Delta_lambda
    Delta_E = h * c * dlambda / (lam**2)
    g = Delta_E / (mu_B * B)

    g_mean = ufloat(g.mean(), sem(g))

    n = np.arange(len(g)) + 1

    fig, ax = plt.subplots()
    ax.plot(n, g, "k.", label="Messwerte")
    ax.axhline(g_mean.n, label="Mittelwert")
    ax.axhspan((g_mean.n - g_mean.s),
               (g_mean.n + g_mean.s), alpha=.5,
               label="Standardabweichung")
    ax.set_ylabel(r"$g_J$")
    ax.set_xlabel("Nummer der Linie auf dem Interferenzbild")
    ax.legend()
    plt.savefig("build/plt_{}_g.png".format(name))


    lout.latex_val("{}_I".format(name), I, r"\ampere", "0")
    lout.latex_val("{}_B".format(name), B * 1e3, r"\milli\tesla", "0")
    lout.latex_val("{}_g".format(name), g_mean)

"""
rot_null_spectrum = im_rot_null[1500, :]
rot_aufspaltung_spectrum = im_rot_aufspaltung[1500, :]
blau_sigma_null_spectrum = im_blau_sigma_null[1500, :]
blau_sigma_aufspaltung_spectrum = im_blau_sigma_aufspaltung[1500, :]
blau_pi_null_spectrum = im_blau_pi_null[1500, :]
blau_pi_aufspaltung_spectrum = im_blau_pi_aufspaltung[1500, :]

save_peak_plot(rot_null_spectrum, rot_null_peaks, "rot_null", im_rot_null)
save_peak_plot(rot_aufspaltung_spectrum, rot_aufspaltung_peaks,
               "rot_aufspaltung", im_rot_aufspaltung)
save_peak_plot(blau_sigma_null_spectrum, blau_sigma_null_peaks,
               "blau_sigma_null", im_blau_sigma_null)
save_peak_plot(blau_sigma_aufspaltung_spectrum, blau_sigma_aufspaltung_peaks,
               "blau_sigma_aufspaltung", im_blau_sigma_aufspaltung)
save_peak_plot(blau_pi_null_spectrum, blau_pi_null_peaks,
               "blau_pi_null", im_blau_pi_null)
save_peak_plot(blau_pi_aufspaltung_spectrum, blau_pi_aufspaltung_peaks,
               "blau_pi_aufspaltung", im_blau_pi_aufspaltung)
"""

# Auswertung

print("Auswertung")

save_peak_img(im_rot_null, im_rot_aufspaltung,
              rot_null_peaks, rot_aufspaltung_peaks, "rot")
save_peak_img(im_blau_sigma_null, im_blau_sigma_aufspaltung,
              blau_sigma_null_peaks, blau_sigma_aufspaltung_peaks,
              "blau_sigma")
save_peak_img(im_blau_pi_null, im_blau_pi_aufspaltung,
              blau_pi_null_peaks, blau_pi_aufspaltung_peaks, "blau_pi")


rot_ds, rot_Delta_s = get_ds_Delta_s(rot_null_peaks,
                                     rot_aufspaltung_peaks)
blau_sigma_ds, blau_sigma_Delta_s = get_ds_Delta_s(blau_sigma_null_peaks,
                                                   blau_sigma_aufspaltung_peaks)
blau_pi_ds, blau_pi_Delta_s = get_ds_Delta_s(blau_pi_null_peaks,
                                             blau_pi_aufspaltung_peaks)

calc_g(rot_ds, rot_Delta_s, rot_lambda, rot_Delta_lambda, rot_I, "rot")
calc_g(blau_sigma_ds, blau_sigma_Delta_s, blau_lambda,
       blau_Delta_lambda, blau_sigma_I, "blau_sigma")
calc_g(blau_pi_ds, blau_pi_Delta_s, blau_lambda,
       blau_Delta_lambda, blau_pi_I, "blau_pi")
